# Sharipova Liya
# 11-403
# 009

class RationalFraction:
    def __init__(self, m=0, n=0):
        if (m!=0 and n==0):
            raise WrongInput("Wrong input")
        elif(m<0 and n<0):
            self.m=-m
            self.n=-n
        else:
            self.m=m
            self.n=n
    def __reduce__(self):
        i=1
        while i<10:
            if self.m%i==0 and self.n%i==0:
                self.m=self.m/i
                self.n=self.n/i
                i=1
            i+=1
        return RationalFraction(self.m, self.n)
    def __add__(self, other):
        return RationalFraction(self.m*other.n+other.m*self.n, self.n*other.n).__reduce__()
    def __sub__(self, other):
        return RationalFraction(self.m*other.n-other.m*self.n, self.n*other.n).__reduce__()
    def __mul__(self, other):
        return RationalFraction(self.m*other.m, self.n*other.n).__reduce__()
    def __div__(self, other):
        return RationalFraction(self.m*other.n, self.n*other.m).__reduce__()
    def __str__(self):
        return "%s/%s" %(self.m, self.n)
    def value(self):
        return round(float(self.m)/self.n, 3)
    def __eq__(self, other):
        self=self.__reduce__()
        other=other.__reduce__()
        return (self.m==other.m and self.n==other.n)
    def  numberPart(self):
        return self.m//self.n
class WrongInput(Exception):
    def __init__(self, message):
        self.message=message

