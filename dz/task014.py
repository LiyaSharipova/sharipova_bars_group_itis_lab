# -*- coding: utf-8 -*-
# Sharipova Liya
# 11-403
# 014

def convert(rimNumb):
    dict_out = {'I': 1,
                'V': 5,
                'X': 10,
                'L': 50,
                'C': 100,
                'D': 500,
                'M': 1000}
    res=dict_out[rimNumb[0]]
    for i in range(len(rimNumb) - 1):
        if res < dict_out[rimNumb[i + 1]]:
            res = dict_out[rimNumb[i+1]]-res
        else:
            res = dict_out[rimNumb[i + 1]] +res

    print(res)


convert("XXI")
