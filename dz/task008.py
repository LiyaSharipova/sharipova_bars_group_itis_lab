# Sharipova Liya
# 11-403
# 008
import math


class Vector2D:
    def __init__(self, x=0, y=0):
        self.x=x
        self.y=y
    def __add__(self, other):
        return Vector2D(self.x+other.x, self.y+other.y)
    def __sub__(self, other):
        return self.__add__(other.mul(-1))
    def mul(self, rel):
        return Vector2D(self.x*rel, self.y*rel)
    def __str__(self):
        return "(%s; %s)" % (self.x, self.y)
    def __len__(self):
        return round(math.sqrt(self.x*self.x+ self.y*self.y), 3)
    def __mul__(self, other):
        return round(self.x*other.x+self.y*other.y , 3)
    def __eq__(self, other):
        return self.x==other.y and self.y==other.y
