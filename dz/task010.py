# # Sharipova Liya
# # 11-403
# # 010
#
def check(func):
    def wrapper(*args):
        print args
        k=0
        for i in args:
            if (isinstance(i, int)):
                k+=1
            else:
                raise TypeError("Program failed in argument " + str(k))
    return wrapper
@check
def test(a, b, c):
        print a, b, c

try:
    test(1, 1, 1.2)
except TypeError as e:
    print e.message
