# -*- coding: utf-8 -*-
# Sharipova Liya
# 11-403
# 012
import os


class SuperFile:
    def __init__(self, path):
        if (os.path.exists(path)==False):
            open(path, "w").close()
        self.path=path
    def __add__(self, other):
        baseName1=os.path.basename(self.path).split('.')[0]
        baseName2=os.path.basename(other.path).split('.')[0]
        ext=os.path.basename(self.path).split('.')[1]
        resPath=os.path.dirname(self.path)+'/'+ baseName1 + baseName2+'.'+ ext
        res=open(resPath, "w")
        f1=open(self.path)
        f2=open(other.path)
        for line in f1:
           res.write(line)
        for line in f2:
            res.write(line)
        f1.close()
        f2.close()
        res.close()
        return SuperFile(resPath)
    def mulN(self, N):

        resName=os.path.abspath(self.path).split('.')[0]+'mult%s.' % N+ os.path.abspath(self.path).split('.')[1]
        res=open(resName, "a")
        for i in range(N):
            f=open(self.path)
            for line in f:
                res.write(line)
            f.close()

        res.close()
        return SuperFile(resName)
    def println(self, s):
        f=open(self.path, "a")
        f.write(s)


s=SuperFile("C:/Users/Baths/Documents/test1.txt")
s.__add__(SuperFile("C:/Users/Baths/Documents/test1.txt"))
s.mulN(2)
# s.println("sssssss")
print(os.path.dirname(s.path).split('.')[0])