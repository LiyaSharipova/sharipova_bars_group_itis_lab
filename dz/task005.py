# Sharipova Liya
# 11-403
# 005
import math

n= int (input("Enter radius: "))
for i in range(2*n+1):
    for j in range(2*n+1):
        y1=n+math.sqrt(n*n-math.pow(n-j, 2))
        y2=n-math.sqrt(n*n-math.pow(n-j, 2))
        if(y2<=i<=y1):
            print("0"),
        else:
            print("*"),
    print("\n")
