# Sharipova Liya
# 11-403
# 006

a = list(map(int, raw_input().split()))
if (len(a)<3):
    print("List is too short")
else:
    s=a[0]+a[1]+a[2]
    for i in range(2, len(a)-1):
        s1=a[i-1]+a[i]+a[i+1]
        if(s<s1):
            s=s1

    print(s)
