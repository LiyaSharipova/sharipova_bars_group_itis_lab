# -*- coding: utf-8 -*-
# Sharipova Liya
# 11-403
# 011

import os
import re
import shutil
def definePath(path):
    p=re.compile(r'^[A-Z,a-z]:[/,\\]')
    if (p.match(path)):
        return (path)
    elif path.startswith('/') or path.startswith('\\'):
        return (os.curdir+path)
    else:
        return (os.curdir+'/'+path)
def grep(f1, searchTerm, path):
    i=0
    for line in f1:
                if searchTerm in line:
                    print(path)
                    print(i+1)
                    if (i>=2):
                        print(f1[i-2])
                        print(f1[i-1])
                    if(len(f1)>=i+2):
                        print(f1[i+1])
                        print(f1[i+2])
                i+=1
while (True):
    s = raw_input(os.path.abspath(os.curdir) + ">").split()
    if len(s) == 0:
        continue
    elif s[0] == "exit" and len(s) == 1:
        break
    elif s[0] in ("dir", "ls"):
        for f in os.listdir(os.curdir):
            print f
    elif s[0] == "supercopy" and len(s) == 3:
        if os.path.exists(s[1]):
            count = int(s[2])  # TODO check int
            k = 0
            while k < count:
                shutil.copy(s[1],
                            "%s-copy%s%s" % (
                                os.path.splitext(s[1])[0],
                                str(k).zfill(len(s[2])),
                                os.path.splitext((s[1]))[1]
                            )
                            )
                k += 1
    elif s[0] == "cat":
        if s[1] == ">":
            f = open(s[2], "w")
            string = raw_input()
            while string != ":q":
                f.write(string + '\n')
                string = raw_input()
            f.close()
        else:
            f = open(s[1])
            for l in f:
                if l == "":
                    print
                    l.strip("\n")
            f.close()
    elif s[0] == "rm" and len(s) == 2:
        if os.path.exists(s[1]):
            os.remove(s[1])
        else:
            print
            "File %s not found" % s[1]

    # cd , append , grep
    elif s[0]=="cd" and len(s) == 2:
        path=definePath(s[1])
        try:
            os.chdir(path)
        except OSError:
            print("No such dir")
    elif s[0]=="append" and len(s)==3:
        path=definePath(s[1])
        text=s[2]
        if(os.path.exists(path)):
            f=open(path, 'a')
            f.write(text)
            f.close()
        else:
            print("No such file")
    elif s[0]=="grep" and len(s)==3:
        path=definePath(s[1])
        string=s[2]
        if(os.path.isfile(path)):
            f=open(path)
            f1=f.readlines()
            grep(f1, string, os.path.basename(path))
        elif(os.path.isdir(path)):
             for dir_path, dirs, file_names in os.walk(path):
                  for file_name in file_names:
                     fullpath = os.path.join(dir_path, file_name)
                     f=open(fullpath)
                     grep(f.readlines(), string)

        else:
            print("No such file or dir")

    else:
        print
        'WAT?'
print
