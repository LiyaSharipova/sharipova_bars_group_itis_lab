from django.conf.urls import include, url

from event.views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^events', show_all_ev , name='events'),
    url(r'^articles', show_all_ar , name='articles'),
    url(r'^event/(?P<post_id>\d+)', 'board.views.event',
        name='choosenEvent'),
    url(r'^article/(?P<post_id>\d+)', 'board.views.article',
        name='choosenArticle'),
    url(r'^profile', 'board.views.profile',
        name='profile'),

]
