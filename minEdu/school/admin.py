from django.contrib import admin
from school.models import *
# Register your models here.
admin.site.register(School)
admin.site.register(Prepod)
admin.site.register(Student)
admin.site.register(Subject)
admin.site.register(Lesson)
admin.site.register(StudentSchedule)
admin.site.register(City)

