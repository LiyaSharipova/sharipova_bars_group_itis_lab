from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from school.models import City, School


def index(request):
    s = "<h1>Hello!</h1><br/> <a href=\"%s\"> Choose city</a>" % reverse("school:city")
    return HttpResponse(s)
def chooseCity(request):
    s="<h1>Choose a city</h2>"
    cities=City.objects.all()

    for city in cities:
        s += "".join("<li><a href=city%s>%s</a></li>" % (str(city.id),city.name))
    return HttpResponse(s)
    # s += "".join(map(lambda x: "<li>%s</li>" % x, cities))
    # return HttpResponse(s)

def all_school(request, city_id):
    s=""
    city1=City.objects.get(id=city_id)
    schools=School.objects.filter(city=city1)
    s += "".join(map(lambda x: "<li>%s ></li>" % x, schools))
    return  HttpResponse(s)
def school_subj(request):
    s=""
    return  HttpResponse(s)
