# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0007_subject'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('level', models.IntegerField()),
                ('prepod', models.ForeignKey(to='school.Prepod')),
                ('school', models.ForeignKey(to='school.School')),
            ],
            options={
                'db_table': 'lesson',
                'verbose_name_plural': '\u041a\u043b\u0430\u0441\u0441\u044b',
            },
        ),
        migrations.CreateModel(
            name='StudentSchedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lesson', models.ForeignKey(to='school.Lesson')),
                ('student', models.ForeignKey(to='school.Student')),
            ],
            options={
                'db_table': 'studentSchedule',
                'verbose_name_plural': '\u041f\u0440\u0435\u0434\u043c\u0435\u0442 \u0441\u0442\u0443\u0434\u0435\u043d\u0442\u0430',
            },
        ),
        migrations.RemoveField(
            model_name='subject',
            name='prepod',
        ),
        migrations.RemoveField(
            model_name='subject',
            name='school',
        ),
        migrations.AddField(
            model_name='subject',
            name='subSubject',
            field=models.ForeignKey(to='school.Subject', null=True),
        ),
    ]
