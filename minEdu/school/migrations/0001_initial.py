# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='School',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.CharField(max_length=50)),
                ('number', models.IntegerField(default=0)),
                ('type', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'school',
                'verbose_name_plural': '\u0428\u043a\u043e\u043b\u044b',
            },
        ),
    ]
