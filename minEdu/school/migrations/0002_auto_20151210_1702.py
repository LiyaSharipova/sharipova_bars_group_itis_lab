# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='school',
            name='type',
            field=models.CharField(max_length=50, choices=[('\u0433\u0438\u043c\u043d', '\u0413\u0438\u043c\u043d\u0430\u0437\u0438\u044f'), ('\u0441\u043e\u0448', '\u0421\u0440\u0435\u0434\u043d\u044f\u044f \u043e\u0431\u0440\u0430\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c\u043d\u0430\u044f \u0448\u043a\u043e\u043b\u0430'), ('\u0438\u043d\u0442', '\u0418\u043d\u0442\u0435\u0440\u043d\u0430\u0442'), ('\u0447\u0430\u0441\u0442\u043d', '\u0427\u0430\u0441\u0442\u043d\u0430\u044f \u0448\u043a\u043e\u043b\u0430')]),
        ),
        migrations.AlterUniqueTogether(
            name='school',
            unique_together=set([('city', 'number')]),
        ),
    ]
