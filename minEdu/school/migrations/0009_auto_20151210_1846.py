# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0008_auto_20151210_1807'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'cities',
                'verbose_name_plural': '\u0413\u043e\u0440\u043e\u0434\u0430',
            },
        ),
        migrations.AlterUniqueTogether(
            name='school',
            unique_together=set([('number',)]),
        ),
        migrations.AddField(
            model_name='school',
            name='city_id',
            field=models.ForeignKey(to='school.City', null=True),
        ),
    ]
