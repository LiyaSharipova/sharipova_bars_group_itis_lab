# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0009_auto_20151210_1846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='school',
            name='city',
            field=models.ForeignKey(to='school.City', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='school',
            unique_together=set([('city', 'number')]),
        ),
        migrations.RemoveField(
            model_name='school',
            name='city_id',
        ),
    ]
