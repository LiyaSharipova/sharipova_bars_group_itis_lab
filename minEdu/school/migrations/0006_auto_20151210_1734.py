# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0005_auto_20151210_1723'),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('age', models.IntegerField()),
                ('city', models.CharField(max_length=50)),
                ('school', models.ForeignKey(to='school.School', null=True)),
            ],
            options={
                'db_table': 'student',
                'verbose_name_plural': '\u0421\u0442\u0443\u0434\u0435\u043d\u0442\u044b',
            },
        ),
        migrations.AlterField(
            model_name='prepod',
            name='name',
            field=models.CharField(max_length=50),
        ),
    ]
