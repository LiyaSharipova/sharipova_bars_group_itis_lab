# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0006_auto_20151210_1734'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('prepod', models.ForeignKey(to='school.Prepod')),
                ('school', models.ForeignKey(to='school.School')),
            ],
            options={
                'db_table': 'subject',
                'verbose_name_plural': '\u041f\u0440\u0435\u0434\u043c\u0435\u0442\u044b',
            },
        ),
    ]
