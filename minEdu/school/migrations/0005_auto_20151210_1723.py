# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0004_prepod'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prepod',
            name='name',
            field=models.CharField(default='\u0433\u0438\u043c\u043d', max_length=50),
        ),
    ]
