# coding=utf-8
from django.db import models
types = (
    (u"гимн", u"Гимназия"), (u"сош", u"Средняя образовательная школа"), (u"инт", u"Интернат"), (u"частн", u"Частная школа"),
)

# Create your models here.
class School(models.Model):
    class Meta:
        db_table = 'school'
        verbose_name_plural = u'Школы'
        unique_together = [ 'city', 'number']
    city = models.ForeignKey('City', null=True)
    # city = models.CharField(max_length=50, blank=False)
    number = models.IntegerField(default=0)
    type = models.CharField(max_length=50, choices=types, default=u"гимн")
    def __unicode__(self):
        return "%s: %s; %s " % (self.city, self.number, self.type)


class Prepod(models.Model):
    class Meta:
        db_table = 'prepod'
        verbose_name_plural = u'Преподаватели'


    city = models.CharField(max_length=50, blank=False)
    school=models.ForeignKey('School',  related_name="school", null=True,)
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return "%s: %s; %s " % (self. name, self.city, self.school)

class Student(models.Model):
    class Meta:
        db_table = 'student'
        verbose_name_plural = u'Студенты'


    name = models.CharField(max_length=50)
    age = models.IntegerField()
    city = models.CharField(max_length=50, blank=False)
    school=models.ForeignKey('School', null=True,)

    def __unicode__(self):
        return "%s: %s; %s ; %s " % (self. name, self.age, self.city, self.school)


class Subject(models.Model):
    class Meta:
        db_table = 'subject'
        verbose_name_plural = u'Предметы'


    name = models.CharField(max_length=50)
    subSubject= models.ForeignKey('Subject', null=True)

    def __unicode__(self):
        return "%s: %s" % (self. name,  self.subSubject)



class Lesson(models.Model):
    class Meta:
        db_table = 'lesson'
        verbose_name_plural = u'Классы'


    name = models.CharField(max_length=50)
    school=models.ForeignKey('School')
    prepod=models.ForeignKey('Prepod')
    level=models.IntegerField()


    def __unicode__(self):
        return "%s: %s; %s ; %s likes" % (self. name,  self.school, self.prepod, self.level)

class StudentSchedule(models.Model):
    class Meta:
        db_table = 'studentSchedule'
        verbose_name_plural = u'Предмет студента'


    student= models.ForeignKey('Student')
    lesson=models.ForeignKey('Lesson')

    def __unicode__(self):
        return "%s: %s" % (self.student, self.lesson)

class City(models.Model):
    class Meta:
        db_table = 'cities'
        verbose_name_plural = u'Города'


    name=models.CharField(max_length=50)


    def __unicode__(self):
        return "%s" % (self.name)
