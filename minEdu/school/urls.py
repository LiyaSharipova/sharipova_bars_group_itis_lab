from django.conf.urls import include, url

from school.views import *

urlpatterns = [
    # url(r'^$', index, name='index'),
    url(r'^city(?P<city_id>\d+)', 'school.views.all_school',
        name='schools'),
    url(r'^city(?P<city_id>\d+)/subjects', 'school.views.school_subj',
        name='subj'),
    url(r'^$', 'school.views.chooseCity' , name='cities'),
]
