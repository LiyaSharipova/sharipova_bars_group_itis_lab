"""dobrye_dela URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:vi
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import TemplateView

from dobrye_dela import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^event/', include('event.urls', namespace="event")),
    url(r'^login/', 'event.views.login', name='login'),
    url(r'^logout/', 'event.views.logout', name="logout"),
    url(r'^profile/', TemplateView.as_view(template_name="event/profile.html"), name="profile"),

]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
