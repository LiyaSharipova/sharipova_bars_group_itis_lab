from pyexpat import model

from django.forms.models import ModelForm
from event.models import Event


class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = ["title", "address", "photo", "description", "score", "private"]
