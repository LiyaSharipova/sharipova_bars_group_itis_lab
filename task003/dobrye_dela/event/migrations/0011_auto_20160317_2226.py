# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0010_auto_20160317_2223'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='done_event1',
            new_name='done_event',
        ),
    ]
