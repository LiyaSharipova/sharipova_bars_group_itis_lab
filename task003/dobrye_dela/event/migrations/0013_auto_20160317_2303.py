# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0012_auto_20160317_2258'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='extraLink',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='fileAttached',
            field=models.FileField(null=True, upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='photo',
            field=models.ImageField(null=True, upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='private',
            field=models.BooleanField(),
        ),
    ]
