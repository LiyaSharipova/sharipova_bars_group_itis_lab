# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0007_auto_20160317_2219'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='done_event',
        ),
        migrations.AddField(
            model_name='userprofile',
            name='done_event1',
            field=models.ForeignKey(related_name='done_event', to='event.Event', null=True),
        ),
    ]
