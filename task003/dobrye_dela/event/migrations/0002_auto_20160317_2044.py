# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organization',
            name='review',
            field=models.ForeignKey(related_name='review', to='event.Review', null=True),
        ),
    ]
