# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0003_auto_20160317_2108'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='organization',
            name='review',
        ),
        migrations.AddField(
            model_name='review',
            name='organization',
            field=models.ForeignKey(to='event.Organization', null=True),
        ),
    ]
