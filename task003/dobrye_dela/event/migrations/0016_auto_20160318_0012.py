# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0015_auto_20160317_2339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='auther',
            field=models.ForeignKey(related_name='auther', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='article',
            name='event',
            field=models.ForeignKey(to='event.Event', null=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='photo',
            field=models.ImageField(null=True, upload_to=b'', blank=True),
        ),
    ]
