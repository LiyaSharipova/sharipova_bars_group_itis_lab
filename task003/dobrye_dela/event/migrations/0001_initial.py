# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pubTime', models.DateTimeField()),
                ('photo', models.ImageField(null=True, upload_to=b'')),
                ('text', models.TextField(max_length=500)),
            ],
            options={
                'db_table': 'article',
                'verbose_name_plural': '\u0421\u0442\u0430\u0442\u044c\u0438',
            },
        ),
        migrations.CreateModel(
            name='ClothCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
                'db_table': 'cloth_category',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438 \u041e\u0434\u0435\u0436\u0434\u044b',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('date', models.DateTimeField()),
                ('photo', models.ImageField(upload_to=b'')),
                ('address', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=400)),
                ('fileAttached', models.FileField(null=True, upload_to=b'')),
                ('extraLink', models.URLField(null=True)),
                ('score', models.IntegerField(default=2)),
                ('private', models.BinaryField(default=False)),
            ],
            options={
                'db_table': 'event',
                'verbose_name_plural': '\u041c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f',
            },
        ),
        migrations.CreateModel(
            name='EventCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('description', models.TextField(max_length=400)),
            ],
            options={
                'db_table': 'event_category',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438 \u0421\u043e\u0431\u044b\u0442\u0438\u0439',
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('likes', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'organization',
                'verbose_name_plural': '\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='Organizator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phoneNumber', models.CharField(max_length=30)),
                ('skypeLogin', models.CharField(max_length=30)),
                ('event', models.ManyToManyField(related_name='org_events', to='event.Event')),
            ],
            options={
                'db_table': 'organizator',
                'verbose_name_plural': '\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0442\u043e\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(max_length=300)),
                ('pubDate', models.DateTimeField()),
            ],
            options={
                'db_table': 'review',
                'verbose_name_plural': '\u041e\u0442\u0437\u044b\u0432\u044b',
            },
        ),
        migrations.CreateModel(
            name='Transport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=30)),
                ('cost', models.FloatField()),
                ('capasity', models.IntegerField(default=1)),
            ],
            options={
                'db_table': 'transport',
                'verbose_name_plural': '\u0412\u0430\u0440\u0438\u0430\u043d\u0442\u044b \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0430',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('photo', models.ImageField(null=True, upload_to=b'')),
                ('city', models.CharField(max_length=30)),
                ('karma', models.IntegerField(default=0)),
                ('done_event', models.ManyToManyField(related_name='done_event', to='event.Event')),
                ('user', models.ForeignKey(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'user',
                'verbose_name_plural': '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0438',
            },
        ),
        migrations.AddField(
            model_name='review',
            name='user',
            field=models.ForeignKey(to='event.UserProfile'),
        ),
        migrations.AddField(
            model_name='organization',
            name='review',
            field=models.ForeignKey(to='event.Review'),
        ),
        migrations.AddField(
            model_name='event',
            name='category',
            field=models.ForeignKey(to='event.EventCategory'),
        ),
        migrations.AddField(
            model_name='article',
            name='auther',
            field=models.ForeignKey(related_name='auther', to='event.UserProfile'),
        ),
        migrations.AddField(
            model_name='article',
            name='event',
            field=models.ForeignKey(to='event.Event'),
        ),
    ]
