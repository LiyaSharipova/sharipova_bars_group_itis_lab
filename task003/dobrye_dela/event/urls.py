from django.conf.urls import include, url
from django.views.generic.base import TemplateView
from django.views.generic.edit import BaseCreateView
from django.views.generic.list import ListView
from event.models import Organization
from event.views import *

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="event/index.html"), name='index'),
    url(r'^organizations$', ListView.as_view(model=Organization,
                                             template_name="event/organization_page.html",
                                             context_object_name="orgs"
                                             ), name='org'),
    url(r'^new$', add_event, name='add_event'),
    url(r'^article/new$', add_article, name='add_article'),
    url(r'^(?P<event_id>\d+)$', event_page, name='event_page'),
    url(r'^article/(?P<article_id>\d+)$', article_page, name='article_page')
    url(r'^events', show_all_ev, name='events'),
    url(r'^articles', show_all_ar, name='articles'),
    url(r'^event/(?P<post_id>\d+)', 'board.views.event',
        name='choosenEvent'),

]
