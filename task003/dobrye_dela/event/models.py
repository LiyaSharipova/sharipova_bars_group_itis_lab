# coding=utf-8
import user

import datetime
from django.contrib.auth.models import User
from django.db import models
# Create your models here.
from django.db.models.fields import DateTimeField


class UserProfile(models.Model):
    class Meta:
        db_table = 'user'
        verbose_name_plural = u'Пользователи'

    user = models.ForeignKey(User, related_name="profile")
    # name = models.CharField(max_length=50)
    # login = models.CharField(max_length=30)
    email = models.CharField(max_length=100)
    # password = models.CharField(max_length=100)
    photo = models.ImageField(null=True)
    city = models.CharField(max_length=30)
    # очки с выполненных дел
    karma = models.IntegerField(default=0)
    # done_event = models.ManyToManyField('Event', related_name="done_event", null=True)
    # done_event = models.ForeignKey('Event', related_name="done_event", null=True)
    # def __unicode__(self):
    #     return "%s: %s; %s " % (self.username, self.city, self.karma)


class Organizator(models.Model):
    class Meta:
        db_table = 'organizator'
        verbose_name_plural = u'Организаторы'

    phoneNumber = models.CharField(max_length=30)
    skypeLogin = models.CharField(max_length=30)
    # organisation = models.ForeignKey('Organization')
    event = models.ManyToManyField('Event', related_name="org_events")

    def __unicode__(self):
        return "%s: %s; " % (self.name, self.organisation)


class Organization(models.Model):
    class Meta:
        db_table = "organization"
        verbose_name_plural = u'Организации'

    name = models.CharField(max_length=100)
    likes = models.IntegerField(default=0)

    def __unicode__(self):
        return " %s ;%s " % (self.name, self.likes)


# отзыв на организацию
class Review(models.Model):
    class Meta:
        db_table = 'review'
        verbose_name_plural = u'Отзывы'

    user = models.ForeignKey(User, related_name="author_of_review")
    # организация
    organization = models.ForeignKey("Organization", null=True)
    text = models.TextField(max_length=500)
    pubDate = models.DateTimeField()

    def __unicode__(self):
        return "%s: %s; %s  " % (self.text, self.user.username, self.pubDate)


# добрые дела, события, благотворительные вечера
class Event(models.Model):
    class Meta:
        db_table = 'event'
        verbose_name_plural = u'Мероприятия'

    title = models.CharField(max_length=50)
    # category = models.ForeignKey('EventCategory')
    date = models.DateTimeField(null=True)
    photo = models.ImageField(null=True, blank=True)
    address = models.CharField(max_length=50)
    # organizator = models.ManyToManyField('Organizator', related_name="organisators")
    # organization = models.ManyToManyField('Organization', null=True)
    description = models.CharField(max_length=400)
    fileAttached = models.FileField(null=True, blank=True)
    extraLink = models.URLField(null=True, blank=True)
    # баллы к карме
    score = models.IntegerField(default=2)
    # видно всем или только зарегистрированным
    private = models.BooleanField()

    def __unicode__(self):
        return "%s: %s; %s ;%s " % (self.title, self.address, self.category.name)


# # тип мероприятия
# class CollectingEvent(Event):
#     item = models.CharField(max_length=30)
#     amount = models.IntegerField()
#
# # тип мероприятия
# class HelpingEvent(Event):
#     transport = models.ForeignKey('Transport')
#     clothType = models.ForeignKey('ClothCategory')
#     physical_difficulty = models.CharField(max_length=20)


# мотивирующие статьи
class Article(models.Model):
    class Meta:
        db_table = 'article'
        verbose_name_plural = u'Статьи'

    title = models.CharField(max_length=20, null=True)
    auther = models.ForeignKey(User, related_name="auther")
    pubTime = models.DateTimeField()
    event = models.ForeignKey('Event', null=True)
    photo = models.ImageField(null=True, blank=True)
    text = models.TextField(max_length=500)

    def __unicode__(self):
        return "%s: %s; %s ;%s " % (self.auther.name, self.event.title, self.text)


# категории мероприятий
class EventCategory(models.Model):
    class Meta:
        db_table = 'event_category'
        verbose_name_plural = u'Категории Событий'

    name = models.CharField(max_length=30)
    description = models.TextField(max_length=400)

    def __unicode__(self):
        return "%s: %s; %s ;%s " % (self.name, self.description)


class ClothCategory(models.Model):
    class Meta:
        db_table = 'cloth_category'
        verbose_name_plural = u'Категории Одежды'

    name = models.CharField(max_length=30)

    def __unicode__(self):
        return "%s " % (self.name)


class Transport(models.Model):
    class Meta:
        db_table = 'transport'
        verbose_name_plural = u'Варианты транспорта'

    type = models.CharField(max_length=30)
    cost = models.FloatField()
    capasity = models.IntegerField(default=1)

    def __unicode__(self):
        return "%s: %s; %s ;%s " % (self.type, self.capasity, self.cost)
