from datetime import datetime
from pickle import GET
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout

# Create your views here.
from event.form import EventForm
from event.models import Event, Article


def login(request):
    if request.user.is_authenticated():
        return redirect(reverse("event:index"))
    if request.method == "GET":
        context = {}
        if "next" in request.GET:
            context["next"] = "?next=" + request.GET["next"]
        return render(request, 'event/login.html')
    elif request.method == "POST":
        user = authenticate(
            username=request.POST["username"],
            password=request.POST["password"]
        )
        if user is not None:
            auth_login(request, user)
            if "next" in request.GET:
                return redirect(request.GET["next"])
            else:
                return redirect(reverse("event:index"))
        else:
            return redirect(reverse("login"))

    else:
        return HttpResponse("405")


def logout(request):
    auth_logout(request)
    return redirect(reverse("login"))


@login_required(login_url=reverse_lazy("login"))
def add_event(request):
    if request.method == "GET":
        f = EventForm()
        return render(request, "event/add_event_page.html", {"f": f})
    elif request.method == "POST":
        f = EventForm(request.POST)
        if f.is_valid():
            ev = f.save()
            return redirect(reverse("event:event_page", args=(ev.id,)))
    else:

        return HttpResponse("405")


def event_page(request, event_id):
    if request.method == "GET":
        ev = Event.objects.get(id=event_id)
        return render(request, "event/event_page.html", {"event": ev})


def article_page(request, article_id):
    if request.method == "GET":
        ar = Article.objects.get(id=article_id)
        return render(request, "event/article_page.html", {"article": ar})


@login_required(login_url=reverse_lazy("login"))
def add_article(request):
    if request.method == "GET":
        return render(request, "event/add_article.html", {"events": Event.objects.all()})

    elif request.method == "POST":
        ar = Article()
        ar.title = request.POST["title"]
        ar.text = request.POST["text"]
        ar.pubTime = datetime.now()
        ar.auther = request.user
        ar.id = request.POST["event"]
        ar.save()
        return redirect(reverse("event:article_page", args=(ar.id,)))
    else:
        return HttpResponse("405")
