from django.contrib import admin
# Register your models here.
from event.models import Event, Organizator, Organization, Article, Review, UserProfile

admin.site.register(Event)
admin.site.register(Organizator)
admin.site.register(Organization)
admin.site.register(Article)
admin.site.register(Review)
admin.site.register(UserProfile)
